package movies.importer;

import java.util.ArrayList;

public class KaggleImporter extends Processor{
	
	public KaggleImporter(String source, String destination) {
		super(source, destination, true);
	}
	
	public ArrayList<String> process(ArrayList<String> a){
		ArrayList<String> movies = new ArrayList<String>();
		
		for(int i = 0; i < a.size(); i++) {
			String[] categories = a.get(i).split("\\t", -1);
			if(categories.length == 21) {
				String year = categories[20];
				String name = categories[15];
				String runTime = categories[13];
				Movie addMovie = new Movie(year, name, runTime, "kaggle");
				movies.add(addMovie.toString());
				}
		}
		return movies;
	}
}
