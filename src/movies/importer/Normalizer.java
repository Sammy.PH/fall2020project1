package movies.importer;
import java.util.ArrayList;
public class Normalizer extends Processor{
	
	public Normalizer(String source, String destination) {
		super(source, destination, false);
	}
	
	public ArrayList<String> process(ArrayList<String> a){
		ArrayList<String> normalizedList = new ArrayList<String>();
		
		for(int i = 0; i < a.size(); i++) {
			String[] categories = a.get(i).split("\\t");
			String year = categories[0];
			String name = categories[1].toLowerCase();
			String[] runtime = categories[2].split(" ");
			String time = runtime[0];
			Movie normalizedMovie = new Movie(year, name, time, "kaggle");
			normalizedList.add(normalizedMovie.toString());
		}
		return normalizedList;
	}
}
