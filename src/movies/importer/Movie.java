package movies.importer;

public class Movie {
	private String releaseYear;
	private String name;
	private String runTime;
	private String source;
	
	//Constructor
	public Movie(String releaseYear, String name, String runTime, String source) {
		this.releaseYear = releaseYear;
		this.name = name;
		this.runTime = runTime;
		this.source = source;
	}
	
	public String getReleaseYear() {
		return releaseYear;
	}
	
	public String getName() {
		return name;
	}
	
	public String getrunTime() {
		return runTime;
	}
	
	public String getSource() {
		return source;
	}
	
	public String toString() {
		return getReleaseYear() + "\t" + getName() + "\t" + getrunTime() + "\t" + getSource(); 
	}
}
