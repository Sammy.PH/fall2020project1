package movies.importer;

import java.util.ArrayList;

public class Deduper extends Processor{
	public Deduper(String source, String destination) {
		super(source, destination, false);
	}
	
	public ArrayList<String> process(ArrayList<String> a){
		ArrayList<String> deduped = new ArrayList<String>();
		for(int i = 0; i < a.size(); i++) {
			String[] splitmovie = a.get(i).split("\\t");
			Movie movies = new Movie(splitmovie[0], splitmovie[1] , splitmovie[2], splitmovie[3]);
			if(!deduped.contains(movies.toString())) {
				deduped.add(movies.toString());
			}
			else {
				String[] secondSplit = deduped.get(deduped.indexOf(movies.toString())).split("\\t");
				int index = deduped.indexOf(movies.toString());
				deduped.set(index, secondSplit[0] + "\t" + secondSplit[1] + "\t" + secondSplit[2] + "\timdb;kaggle");
			}
		}
		return deduped;
	}
}