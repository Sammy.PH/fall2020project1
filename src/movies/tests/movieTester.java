package movies.tests;
import movies.importer.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class movieTester {

	private KaggleImporter kaggle = new KaggleImporter("src\\random\\place", "src\\random\\place");
	private Normalizer normal = new Normalizer("src\\random\\place", "src\\random\\place");
	private Deduper duper = new Deduper("src\\random\\place", "src\\random\\place");
	
	@Test
	public void movieOBJtest() {
		Movie testing = new Movie("2000","boby","134 minute", "kaggle");
		String expectedResult = "2000\tboby\t134 minute\tkaggle";
		assertEquals(expectedResult, testing.toString());
	}
	
	@Test
	public void testKaggle() {
		ArrayList<String> testList = new ArrayList<String>();
		testList.add("Brendan Fraser	John Hannah	Maria Bello	Michelle Yeoh	Jet Li	Russell Wong	\"The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their fair share of experience battling the legions of the undead. Should the fierce monarch prove capable of awakening his powerful terra cotta army, his diabolical plan for world domination will finally be set into motion. Of course, the one factor that this emperor mummy failed to consider while solidifying his power-mad plans was the O'Connells, and before this battle is over, the monstrous monarch will be forced to contend with the one family that isn't frightened by a few rickety reanimated corpses. ~ Jason Buchanan, Rovi\"	Rob Cohen	Simon Duggan	Director Not Available	Action	PG-13 	7/24/2008	112 minutes	Universal Pictures	The Mummy: Tomb of the Dragon Emperor	Alfred Gough	Miles Millar	Writer Not Available	Writer Not Available	2008");
		String ExpectedResult = "2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes\tkaggle";
		assertEquals(ExpectedResult, kaggle.process(testList).get(0));
	}
	
	@Test
	public void testNormalization() {
		ArrayList<String> testList = new ArrayList<String>();
		testList.add("2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes");
		String expectedResult = "2008\tthe mummy: tomb of the dragon emperor\t112\tkaggle";
		assertEquals(expectedResult, normal.process(testList).get(0));
	}
	
	@Test
	public void testDupe() {
		ArrayList<String> testList = new ArrayList<String>();
		testList.add("2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes\tkaggle");
		testList.add("2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes\tkaggle");
		String expectedResult = "2008\tThe Mummy: Tomb of the Dragon Emperor\t112 minutes\timdb;kaggle";
		assertEquals(expectedResult, duper.process(testList).get(0));
	}
	
}
