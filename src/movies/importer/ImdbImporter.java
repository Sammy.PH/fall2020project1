package movies.importer;
import java.util.ArrayList;
public class ImdbImporter extends Processor {

	public ImdbImporter(String source, String destination) {
		super(source, destination, true);
	}
	
	public ArrayList<String> process(ArrayList<String> a){
		ArrayList<String> movies = new ArrayList<String>();
		for(int i = 0; i < a.size(); i++) {
			String[] categories = a.get(i).split("\\t", -1);
			if(categories.length == 22) {
				String year = categories[3];
				String name = categories[1];
				String runTime = categories[6];
				Movie addMovie = new Movie(year, name, runTime, "imdb");
				movies.add(addMovie.toString());
			}
		}
		return movies;
	}
}