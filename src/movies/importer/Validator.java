package movies.importer;

import java.util.ArrayList;

public class Validator extends Processor{
	
	public Validator(String source, String destination) {
		super(source, destination, false);
	}
	
	public ArrayList<String> process(ArrayList<String> a){
		ArrayList<String> normalizedList = new ArrayList<String>();
		
		for(int i = 0; i < a.size(); i++) {
			String[] categories = a.get(i).split("\\t");
			if(categories[0].equals("") && categories[1].equals("") && categories[2].equals("")) {
				//NOthing
			}
			else {
				Movie validatedMovie;
				try {
					int year = Integer.parseInt(categories[0]);
					int runTime = Integer.parseInt(categories[2]);
					String name = categories[1];
					validatedMovie = new Movie(String.valueOf(year), name, String.valueOf(runTime), "imdb");
				}
				catch(Exception e) {
					System.out.println("not convertable");
					String name = categories[1];
					String year = categories[0];
					String runTime = categories[2];
					validatedMovie = new Movie(year, name, runTime, "imdb");
				}
				normalizedList.add(validatedMovie.toString());
			}
		}
		return normalizedList;
	}
}