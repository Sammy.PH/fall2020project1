package movies.importer;
import java.io.*;
public class ImportPipeline {

	public static void main(String[] args) throws IOException{
		Processor[] allprocessors = new Processor[5];
		allprocessors[0] = new KaggleImporter("..\\..\\..\\texts\\KaggleSmallFile.txt", "..\\..\\..\\destination");
		allprocessors[1] = new ImdbImporter("..\\..\\..\\texts\\ImdbSmallFile.txt", "..\\..\\..\\destination");
		allprocessors[2] = new Normalizer("..\\..\\..\\destination", "..\\..\\..\\normalized");
		allprocessors[3] = new Validator("..\\..\\..\\normalized", "..\\..\\..\\validated");
		allprocessors[4] = new Deduper("..\\..\\..\\validated", "..\\..\\..\\validated");
		processAll(allprocessors);
	}
	
	public static void processAll(Processor[] a) throws IOException{
		for(int i = 0; i < a.length; i++) {
			a[i].execute();
		}
	}

}
